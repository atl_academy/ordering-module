﻿using Microsoft.EntityFrameworkCore;
using ejdahaorder_backend.FoodCategories.Domain;
using ejdahaorder_backend.Foods.Domain;
using ejdahaorder_backend.Orders.Domain;
using ejdahaorder_backend.OrderItems.Domain;

namespace ejdahaorder_backend.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region Order
            modelBuilder.Entity<Order>(order =>
            {
                order.ToTable("orders");
                order.HasKey(e => e.Id).HasName("pk_orders");

                order.Property(e => e.Id).HasColumnName("id").IsRequired();
                order.Property(e => e.FirstName).HasColumnName("first_name").IsRequired();
                order.Property(e => e.LastName).HasColumnName("last_name").IsRequired();
                order.Property(e => e.Email).HasColumnName("email");
                order.Property(e => e.PhoneNumber).HasColumnName("phone_number").IsRequired();
                order.Property(e => e.Address).HasColumnName("address").IsRequired();
                order.Property(e => e.TotalPrice).HasColumnName("total_price").IsRequired();
                order.Property(e => e.OrderDate).HasColumnName("order_date").IsRequired();
            });
            #endregion

            #region FoodCategory
            modelBuilder.Entity<FoodCategory>(foodcategory =>
            {
                foodcategory.ToTable("food_categories");
                foodcategory.HasKey(e => e.Id).HasName("pk_food_categories");
                foodcategory.HasIndex(e => e.Name).IsUnique().HasName("uk_food_categories_name");

                foodcategory.Property(e => e.Id).HasColumnName("id").IsRequired();
                foodcategory.Property(e => e.Name).HasColumnName("name").IsRequired();

            });
            #endregion

            #region Food
            modelBuilder.Entity<Food>(food =>
            {
                food.ToTable("foods");
                food.HasKey(e => e.Id).HasName("pk_foods");

                food.Property(e => e.Id).HasColumnName("id").IsRequired();
                food.Property(e => e.Name).HasColumnName("name").IsRequired();
                food.Property(e => e.UnitPrice).HasColumnName("unit_price").IsRequired();
                food.Property(e => e.FoodCategoryId).HasColumnName("food_category_id").IsRequired();
                food.Property(e => e.Image).HasColumnName("image");
                food.Property(e => e.Description).HasColumnName("description");

                food.HasOne(e => e.FoodCategory).WithMany().HasForeignKey(e => e.FoodCategoryId).HasConstraintName("fk_foods_food_categories_id");
            });
            #endregion

            #region OrderItem

            modelBuilder.Entity<OrderItem>(
                orderItem =>
                {
                    orderItem.ToTable("order_items");
                    orderItem.HasKey(e => e.Id).HasName("pk_order_items");
                    orderItem.HasIndex(e => new { e.OrderId, e.FoodId }).IsUnique().HasName("uk_orders_foods_order_id_food_id");
                    orderItem.Property(e => e.Id).HasColumnName("id").IsRequired();
                    orderItem.Property(e => e.OrderId).HasColumnName("order_id").IsRequired();
                    orderItem.Property(e => e.FoodId).HasColumnName("food_id").IsRequired();
                    orderItem.Property(e => e.Quantity).HasColumnName("quantity").IsRequired();
                    orderItem.HasOne(e => e.Order).WithMany().HasForeignKey(e => e.OrderId).HasConstraintName("fk_orders_foods_order_id");
                    orderItem.HasOne(e => e.Food).WithMany().HasForeignKey(e => e.FoodId).HasConstraintName("fk_orders_foods_food_id");
                }
                );

            #endregion
        }

        public DbSet<Order> Orders { get; set; }
        public DbSet<FoodCategory> FoodCategories { get; set; }
        public DbSet<Food> Foods { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }

    }
}
