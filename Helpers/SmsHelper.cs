﻿using Microsoft.IdentityModel.Protocols;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ejdahaorder_backend.Helpers
{
    public class SmsHelper
    {
        public void Send(string to,string body)
        {
            //string URL = $"https://api.msm.az/sendsms?user=dostnetapi&password=laptopcomp9&gsm={to}&from=DostNet&text={body}";
            string webTarget = $"http://api.msm.az/sendsms?user=dostnetapi&password=laptopcomp9&gsm={to}&from=DostNet&text={body}";
            
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(webTarget);
            req.Method = "GET";
            req.Credentials = CredentialCache.DefaultCredentials; 
            HttpWebResponse res = (HttpWebResponse)req.GetResponse();
            res.Close();
        }
    }

}

