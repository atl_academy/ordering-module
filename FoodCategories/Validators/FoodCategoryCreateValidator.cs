﻿using ejdahaorder_backend.FoodCategories.Dto;
using FluentValidation;

namespace ejdahaorder_backend.FoodCategories.Validators
{
    public class FoodCategoryCreateValidator : AbstractValidator<FoodCategoryCreateDto>
    {
        public FoodCategoryCreateValidator()
        {
            RuleFor(e => e.Name).NotNull().NotEmpty().WithMessage("Name is required!");
        }
    }
}
