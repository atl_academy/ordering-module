﻿using ejdahaorder_backend.FoodCategories.Dto;
using FluentValidation;

namespace ejdahaorder_backend.FoodCategories.Validators
{
    public class FoodCategoryUpdateValidator : AbstractValidator<FoodCategoryUpdateDto>
    {
        public FoodCategoryUpdateValidator()
        {
            RuleFor(e => e.Name).NotNull().NotEmpty().WithMessage("Name is required!");
        }
    }
}
