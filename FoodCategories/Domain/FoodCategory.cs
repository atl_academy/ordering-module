﻿using ejdahaorder_backend.Shared;

namespace ejdahaorder_backend.FoodCategories.Domain
{
    public class FoodCategory:BaseEntity
    {
        public string Name { get; set; }
    }
}
