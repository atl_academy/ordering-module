﻿using ejdahaorder_backend.FoodCategories.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ejdahaorder_backend.FoodCategories
{
    public interface IFoodCategoryService
    {
        Task<IEnumerable<FoodCategoryDto>> FindListAsync();        
        Task CreateAsync(FoodCategoryCreateDto foodCategoryCreateDto);
        Task UpdateAsync(FoodCategoryUpdateDto foodCategoryUpdateDto);
        Task DeleteAsync(int id);
    }
}
