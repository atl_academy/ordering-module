﻿using ejdahaorder_backend.FoodCategories.Dto;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ejdahaorder_backend.FoodCategories
{
    public class FoodCategoriesController : BaseController
    {

        private readonly IFoodCategoryService foodCategoryService;

        public FoodCategoriesController(IFoodCategoryService foodCategoryService)
        {
            this.foodCategoryService = foodCategoryService;
        }


        [HttpGet]
        public async Task<IActionResult> FindListAsync()
        {
            return Ok(await foodCategoryService.FindListAsync());
        }


        [HttpPost]
        public async Task<IActionResult> CreateAsync([FromBody] FoodCategoryCreateDto foodCategoryCreateDto)
        {
            await foodCategoryService.CreateAsync(foodCategoryCreateDto);
            return Ok();
        }


        [HttpPut]
        public async Task<IActionResult> UpdateAsync([FromBody] FoodCategoryUpdateDto foodCategoryUpdateDto)
        {
            await foodCategoryService.UpdateAsync(foodCategoryUpdateDto);
            return Ok();
        }


        [HttpDelete]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            await foodCategoryService.DeleteAsync(id);
            return Ok();
        }
    }
}