﻿using ejdahaorder_backend.Shared;

namespace ejdahaorder_backend.FoodCategories.Dto
{
    public class FoodCategoryDto : BaseDto
    {
        public string Name { get; set; }
    }
}
