﻿namespace ejdahaorder_backend.FoodCategories.Dto
{
    public class FoodCategoryCreateDto
    {
        public string Name { get; set; }
    }
}
