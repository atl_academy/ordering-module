﻿namespace ejdahaorder_backend.Foods.Dto
{
    public class FoodCreateDto
    {
        public string Name { get; set; }
        public double UnitPrice { get; set; }

        public int FoodCategoryId { get; set; }
                
        public string Description { get; set; }
    }
}
