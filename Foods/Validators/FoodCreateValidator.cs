﻿using ejdahaorder_backend.Foods.Dto;
using FluentValidation;

namespace ejdahaorder_backend.Foods.Validators
{
    public class FoodCreateValidator : AbstractValidator<FoodCreateDto>
    {
        public FoodCreateValidator()
        {
            RuleFor(e => e.Name).NotNull().NotEmpty().WithMessage("Name is required!");
            RuleFor(e => e.FoodCategoryId).GreaterThan(0).WithMessage("FoodCategory Id is required!");
            RuleFor(e => e.UnitPrice).GreaterThan(0).WithMessage("UnitPrice is required!");
        }
    }
}
