﻿using ejdahaorder_backend.Foods.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ejdahaorder_backend.Foods
{
    public interface IFoodService
    {
        Task<IEnumerable<FoodDto>> FindListAsync();
        Task<FoodUpdateDto> FindByIdAsync(int id);
        Task CreateAsync(FoodCreateDto foodCreateDto);
        Task UpdateAsync(FoodUpdateDto foodUpdateDto);
        Task DeleteAsync(int id);
    }
}
