﻿using System.Threading.Tasks;
using ejdahaorder_backend.Foods.Dto;
using Microsoft.AspNetCore.Mvc;

namespace ejdahaorder_backend.Foods
{
    public class FoodsController : BaseController
    {
        private readonly IFoodService foodService;

        public FoodsController(IFoodService foodService)
        {
            this.foodService = foodService;
        }


        [HttpGet]
        public async Task<IActionResult> FindListAsync()
        {
            return Ok(await foodService.FindListAsync());
        }


        [HttpGet]
        public async Task<IActionResult> FindByIdAsync(int id)
        {
            return Ok(await foodService.FindByIdAsync(id));
        }


        [HttpPost]
        public async Task<IActionResult> CreateAsync([FromBody] FoodCreateDto foodCreateDto)
        {
            await foodService.CreateAsync(foodCreateDto);
            return Ok();
        }


        [HttpPut]
        public async Task<IActionResult> UpdateAsync([FromBody] FoodUpdateDto foodUpdateDto)
        {
            await foodService.UpdateAsync(foodUpdateDto);
            return Ok();
        }


        [HttpDelete]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            await foodService.DeleteAsync(id);
            return Ok();
        }
    }
}