﻿using AutoMapper;
using ejdahaorder_backend.Data;
using ejdahaorder_backend.Foods.Domain;
using ejdahaorder_backend.Foods.Dto;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ejdahaorder_backend.Foods
{
    public class FoodService:IFoodService
    {
        private readonly IMapper mapper;
        private readonly DataContext dataContext;
        public FoodService(DataContext dataContext, IMapper mapper)
        {
            this.dataContext = dataContext;
            this.mapper = mapper;
        }

        public async Task<IEnumerable<FoodDto>> FindListAsync()
        {
            return mapper.Map<IList<FoodDto>>(await dataContext.Foods.Include(e => e.FoodCategory).ToListAsync());
        }
        
        public async Task<FoodUpdateDto> FindByIdAsync(int id)
        {
            return mapper.Map<FoodUpdateDto>(await dataContext.Foods.Include(e => e.FoodCategory).FirstOrDefaultAsync(
                e => e.Id == id));
        }

        public async Task CreateAsync(FoodCreateDto foodCreateDto)
        {
            Food food= mapper.Map<Food>(foodCreateDto);

            food.FoodCategory= await dataContext.FoodCategories.FindAsync(food.Id);

            dataContext.Foods.Add(food);

            await dataContext.SaveChangesAsync();
        }

        public async Task UpdateAsync(FoodUpdateDto foodUpdateDto)
        {
            //Food food = await dataContext.Foods.FindAsync(foodUpdateDto.Id);
            dataContext.Foods.Update(new Food { Id = foodUpdateDto.Id }).CurrentValues.SetValues(mapper.Map<Food>(foodUpdateDto));

            await dataContext.SaveChangesAsync();
        }

        public async Task DeleteAsync(int id)
        {            
            dataContext.Foods.Remove(new Food { Id = id });

            await dataContext.SaveChangesAsync();
        }
    }
}
