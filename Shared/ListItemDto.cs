﻿namespace ejdahaorder_backend.Shared
{
    public class ListItemDto
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}
