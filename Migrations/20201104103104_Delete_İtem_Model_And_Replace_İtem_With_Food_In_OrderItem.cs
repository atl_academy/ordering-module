﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ejdahaorder_backend.Migrations
{
    public partial class Delete_İtem_Model_And_Replace_İtem_With_Food_In_OrderItem : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "fk_order_items_item_id",
                table: "order_items");

            migrationBuilder.DropForeignKey(
                name: "fk_order_items_order_id",
                table: "order_items");

            migrationBuilder.DropTable(
                name: "items");

            migrationBuilder.DropIndex(
                name: "IX_order_items_item_id",
                table: "order_items");

            migrationBuilder.DropIndex(
                name: "uk_order_items_order_id_item_id",
                table: "order_items");

            migrationBuilder.DropColumn(
                name: "item_id",
                table: "order_items");

            migrationBuilder.AddColumn<int>(
                name: "food_id",
                table: "order_items",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "quantity",
                table: "order_items",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_order_items_food_id",
                table: "order_items",
                column: "food_id");

            migrationBuilder.CreateIndex(
                name: "uk_orders_foods_order_id_food_id",
                table: "order_items",
                columns: new[] { "order_id", "food_id" },
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "fk_orders_foods_food_id",
                table: "order_items",
                column: "food_id",
                principalTable: "foods",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "fk_orders_foods_order_id",
                table: "order_items",
                column: "order_id",
                principalTable: "orders",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "fk_orders_foods_food_id",
                table: "order_items");

            migrationBuilder.DropForeignKey(
                name: "fk_orders_foods_order_id",
                table: "order_items");

            migrationBuilder.DropIndex(
                name: "IX_order_items_food_id",
                table: "order_items");

            migrationBuilder.DropIndex(
                name: "uk_orders_foods_order_id_food_id",
                table: "order_items");

            migrationBuilder.DropColumn(
                name: "food_id",
                table: "order_items");

            migrationBuilder.DropColumn(
                name: "quantity",
                table: "order_items");

            migrationBuilder.AddColumn<int>(
                name: "item_id",
                table: "order_items",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "items",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    food_id = table.Column<int>(type: "int", nullable: false),
                    quantity = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_items", x => x.id);
                    table.ForeignKey(
                        name: "fk_items_foods_id",
                        column: x => x.food_id,
                        principalTable: "foods",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_order_items_item_id",
                table: "order_items",
                column: "item_id");

            migrationBuilder.CreateIndex(
                name: "uk_order_items_order_id_item_id",
                table: "order_items",
                columns: new[] { "order_id", "item_id" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_items_food_id",
                table: "items",
                column: "food_id");

            migrationBuilder.AddForeignKey(
                name: "fk_order_items_item_id",
                table: "order_items",
                column: "item_id",
                principalTable: "items",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "fk_order_items_order_id",
                table: "order_items",
                column: "order_id",
                principalTable: "orders",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
