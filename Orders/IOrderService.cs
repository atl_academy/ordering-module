﻿using ejdahaorder_backend.Orders.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ejdahaorder_backend.Orders
{
    public interface IOrderService
    {
        Task<IEnumerable<OrderDto>> FindListAsync();
        Task<OrderUpdateDto> FindByIdAsync(int id);
        Task CreateAsync(OrderCreateDto order);
        Task UpdateAsync(OrderUpdateDto order);
        Task DeleteAsync(int id);
    }
}
